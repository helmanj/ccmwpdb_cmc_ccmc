﻿CREATE TABLE [dbo].[comment] (
    [comment_ID]           INT            IDENTITY (1, 1) NOT NULL,
    [comment_post_ID]      INT            NULL,
    [comment_author]       INT            NULL,
    [comment_author_email] NVARCHAR (200) NULL,
    [comment_date]         DATETIME       NULL,
    [comment_content]      NVARCHAR (512) NULL,
    [comment_approved]     INT            NULL,
    [comment_parent]       INT            NULL,
    CONSTRAINT [PK_comment] PRIMARY KEY CLUSTERED ([comment_ID] ASC) WITH (FILLFACTOR = 90)
);

