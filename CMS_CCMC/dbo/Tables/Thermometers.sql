﻿CREATE TABLE [dbo].[Thermometers] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [Title]        NTEXT         NULL,
    [Status]       NVARCHAR (10) NULL,
    [DateUpdated]  NVARCHAR (20) NULL,
    [Description]  NTEXT         NULL,
    [Floor]        NTEXT         NULL,
    [Ceiling]      NTEXT         NULL,
    [CurrentValue] NTEXT         NULL,
    [Percentage]   INT           NULL,
    [City]         NTEXT         NULL,
    [State]        NTEXT         NULL,
    [Location]     NTEXT         NULL,
    [Category]     NVARCHAR (50) NULL,
    [Weight]       INT           NULL,
    CONSTRAINT [PK__Thermome__3214EC2709A971A2] PRIMARY KEY CLUSTERED ([ID] ASC)
);

