﻿CREATE TABLE [dbo].[testpost] (
    [post_ID]       INT            IDENTITY (1, 1) NOT NULL,
    [post_slug]     NVARCHAR (256) NULL,
    [post_author]   NVARCHAR (256) NULL,
    [post_date]     DATETIME       NULL,
    [post_content]  NTEXT          NULL,
    [post_category] NVARCHAR (256) NULL,
    [post_title]    NVARCHAR (256) NULL,
    [post_excerpt]  NTEXT          NULL,
    [post_status]   INT            NULL,
    [post_type]     INT            NULL,
    CONSTRAINT [PK_testpost] PRIMARY KEY CLUSTERED ([post_ID] ASC)
);

