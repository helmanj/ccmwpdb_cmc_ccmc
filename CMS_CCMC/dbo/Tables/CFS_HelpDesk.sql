﻿CREATE TABLE [dbo].[CFS_HelpDesk] (
    [CFS_HelpDesk_ID] INT            IDENTITY (1, 1) NOT NULL,
    [Ticket_ID]       NVARCHAR (8)   NULL,
    [Ticket_Text]     NVARCHAR (MAX) NULL,
    [Creation_Date]   DATETIME       CONSTRAINT [DF_CFS_HelpDesk_Creation_Date] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_CFS_HelpDesk] PRIMARY KEY CLUSTERED ([CFS_HelpDesk_ID] ASC)
);

