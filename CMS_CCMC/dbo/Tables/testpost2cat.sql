﻿CREATE TABLE [dbo].[testpost2cat] (
    [ID]      INT IDENTITY (1, 1) NOT NULL,
    [cat_ID]  INT NULL,
    [post_ID] INT NULL,
    CONSTRAINT [PK_testpost2cat] PRIMARY KEY CLUSTERED ([ID] ASC)
);

