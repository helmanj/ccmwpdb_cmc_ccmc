﻿CREATE TABLE [dbo].[announcement] (
    [announcement_ID]            INT            IDENTITY (1, 1) NOT NULL,
    [announcement_date]          DATETIME       NULL,
    [announcement_cust_icon_url] NVARCHAR (100) NULL,
    [announcement_doc_url]       NVARCHAR (256) NULL,
    [announcement_title]         NVARCHAR (256) NULL,
    [announcement_category]      NVARCHAR (50)  NULL,
    CONSTRAINT [PK_announcement] PRIMARY KEY CLUSTERED ([announcement_ID] ASC) WITH (FILLFACTOR = 90)
);

