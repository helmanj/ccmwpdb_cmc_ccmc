﻿CREATE TABLE [dbo].[category] (
    [category_ID]          INT            IDENTITY (1, 1) NOT NULL,
    [category_Name]        NVARCHAR (50)  NULL,
    [category_Description] NVARCHAR (256) NULL,
    [category_Displayname] NVARCHAR (50)  NULL,
    CONSTRAINT [PK_category] PRIMARY KEY CLUSTERED ([category_ID] ASC) WITH (FILLFACTOR = 90)
);

