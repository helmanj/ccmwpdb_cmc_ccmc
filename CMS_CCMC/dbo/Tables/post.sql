﻿CREATE TABLE [dbo].[post] (
    [post_ID]        INT            IDENTITY (1, 1) NOT NULL,
    [post_slug]      NVARCHAR (256) NULL,
    [post_author]    INT            NULL,
    [post_date]      DATETIME       NULL,
    [post_content]   NTEXT          NULL,
    [post_title]     NVARCHAR (256) NULL,
    [post_category]  NVARCHAR (50)  NULL,
    [post_excerpt]   NTEXT          NULL,
    [post_status]    INT            NULL,
    [post_password]  NVARCHAR (20)  NULL,
    [post_name]      NVARCHAR (256) NULL,
    [comment_status] INT            NULL,
    [post_modified]  DATETIME       NULL,
    [post_type]      INT            NULL,
    [post_parent]    INT            NULL,
    [post_order]     INT            NULL,
    CONSTRAINT [PK_post] PRIMARY KEY CLUSTERED ([post_ID] ASC) WITH (FILLFACTOR = 90)
);

