﻿CREATE TABLE [dbo].[Misc] (
    [ID]          INT           IDENTITY (0, 1) NOT NULL,
    [Title]       NTEXT         NULL,
    [Type]        NVARCHAR (50) NULL,
    [Description] NTEXT         NULL,
    [Data1]       NTEXT         NULL,
    [Data2]       NTEXT         NULL,
    [Data3]       NTEXT         NULL,
    [Value1]      INT           NULL,
    CONSTRAINT [PK_Misc] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

