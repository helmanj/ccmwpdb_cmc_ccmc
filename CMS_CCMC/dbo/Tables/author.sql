﻿CREATE TABLE [dbo].[author] (
    [author_ID]         INT              IDENTITY (1, 1) NOT NULL,
    [author_login]      NVARCHAR (50)    NOT NULL,
    [author_password]   NVARCHAR (50)    NULL,
    [author_nicename]   NVARCHAR (50)    NULL,
    [author_email]      NVARCHAR (200)   NULL,
    [author_url]        NVARCHAR (200)   NULL,
    [author_registered] BIT              NULL,
    [activation_key]    UNIQUEIDENTIFIER CONSTRAINT [DF_user_activation_key] DEFAULT (newid()) ROWGUIDCOL NULL,
    [author_status]     INT              NULL,
    [display_name]      NVARCHAR (20)    NULL,
    CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED ([author_ID] ASC, [author_login] ASC) WITH (FILLFACTOR = 90)
);

