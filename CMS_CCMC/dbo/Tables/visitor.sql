﻿CREATE TABLE [dbo].[visitor] (
    [visitor_ID]     INT            IDENTITY (1, 1) NOT NULL,
    [visitor_name]   NVARCHAR (100) NULL,
    [organization]   NVARCHAR (100) NULL,
    [destination]    NVARCHAR (100) NULL,
    [origin]         NVARCHAR (100) NULL,
    [arrival_date]   DATETIME       NULL,
    [departure_date] DATETIME       NULL,
    CONSTRAINT [PK_visitor] PRIMARY KEY CLUSTERED ([visitor_ID] ASC) WITH (FILLFACTOR = 90)
);

