﻿CREATE TABLE [dbo].[logFile] (
    [logID]        INT           IDENTITY (1, 1) NOT NULL,
    [userID]       VARCHAR (50)  NULL,
    [O]            VARCHAR (50)  NULL,
    [OU]           VARCHAR (50)  NULL,
    [jobclass]     VARCHAR (50)  NULL,
    [l]            NVARCHAR (50) NULL,
    [givenName]    NVARCHAR (50) NULL,
    [sn]           NVARCHAR (50) NULL,
    [company]      NVARCHAR (50) NULL,
    [emailAddress] VARCHAR (50)  NULL,
    [dateTime]     DATETIME      NULL,
    CONSTRAINT [PK_logFile] PRIMARY KEY CLUSTERED ([logID] ASC) WITH (FILLFACTOR = 90)
);

