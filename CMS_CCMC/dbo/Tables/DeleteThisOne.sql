﻿CREATE TABLE [dbo].[DeleteThisOne]
(
	deleteID INT NOT NULL PRIMARY KEY,
	testcolumn varchar(10),
	similarcolumnrenamed nvarchar(20),
	anothertest varchar(994),
	somecolumn bit
)
