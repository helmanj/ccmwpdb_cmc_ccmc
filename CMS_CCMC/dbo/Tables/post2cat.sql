﻿CREATE TABLE [dbo].[post2cat] (
    [ID]      INT IDENTITY (1, 1) NOT NULL,
    [cat_ID]  INT NULL,
    [post_ID] INT NULL,
    CONSTRAINT [PK_post2cat] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

