﻿ALTER ROLE [db_owner] ADD MEMBER [CMSAdmin];


GO
ALTER ROLE [db_owner] ADD MEMBER [CCM\Helman];


GO
ALTER ROLE [db_datareader] ADD MEMBER [italert_reader];


GO
ALTER ROLE [db_datareader] ADD MEMBER [CCM\Domain Admins];


GO
ALTER ROLE [db_datareader] ADD MEMBER [CCM\G_IT_Notifier];


GO
ALTER ROLE [db_datareader] ADD MEMBER [ccmcuser];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [CCM\Domain Admins];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [CCM\G_IT_Notifier];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [ccmcuser];


GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [italert_reader];

